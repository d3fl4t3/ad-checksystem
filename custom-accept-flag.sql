drop function if exists accept_flag(integer, text);

create function accept_flag(team_id integer, flag_data text) returns record as $$
<<my>>
declare
  flag   flags%rowtype;
  round  rounds.n%type;
  amount stolen_flags.amount%type;

  attacker_pos smallint;
  victim_pos   smallint;
  amount_max   float8;
  teams_count  smallint;

  service_active boolean;
begin
  select * from flags where data = flag_data into flag;

  if not found then return row(false, 'Denied: no such flag'); end if;
  if team_id = flag.team_id then return row(false, 'Denied: invalid or own flag'); end if;
  if flag.expired then return row(false, 'Denied: flag is too old'); end if;

  select now() between coalesce(ts_start, '-infinity') and coalesce(ts_end, 'infinity')
  from services where id = flag.service_id into service_active;
  if not service_active then return row(false, 'Denied: service inactive'); end if;

  perform * from stolen_flags as sf where sf.data = flag_data and sf.team_id = accept_flag.team_id;
  if found then return row(false, 'Denied: you already submitted this flag'); end if;

  select max(s.round) into round from scoreboard as s;
  select n from scoreboard as s where s.round = my.round - 1 and s.team_id = accept_flag.team_id into attacker_pos;
  select n from scoreboard as s where s.round = my.round - 1 and s.team_id = flag.team_id into victim_pos;

  select count(*) from teams into teams_count;
  select flag_base_amount into amount_max
  from service_activity as sa
  where sa.service_id = flag.service_id and sa.round = flag.round;

  amount = case when attacker_pos > victim_pos
    then amount_max * (attacker_pos - victim_pos + 3)
    else amount_max
  end;

  select max(n) into round from rounds;
  insert into stolen_flags (data, team_id, round, amount)
    values (flag_data, team_id, round, amount) on conflict do nothing;
  if not found then return row(false, 'Denied: you already submitted this flag'); end if;

  return row(true, null, round, flag.team_id, flag.service_id, amount);
end;
$$ language plpgsql;
