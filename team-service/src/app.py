from flask import Flask, request, render_template, send_file
import hashlib
import hmac
import time
import os

BOT_TOKEN = os.environ["BOT_TOKEN"]

app = Flask(__name__)

TEAMS = {
    "hleb": ["KillingInTheNameOf", "yalegko"],
    "umom": ["6488414793", "ilyatomskikh", "Groug1305", "1087614186", "minhson2202"],
    "vinograd": ["m2mkat", "kosmos30732", "asrkmva", "WNReverz", "holeks"],
    "plov": ["alskv13", "ivchenko_ni", "rimodar_v", "Thai_Son_185", "koynov_igor"],
    "team1": ["cravtos", "lsava", "fantsk1", "1023856053", "Snegovichok_Pro", "asalya_s20"],
    "void": ["ForYouMyDearFriendX", "raptura_zxc"],
}

@app.route("/archive")
def index():
    return render_template("index.html")

@app.route("/archive/login")
def login():
    # check telegram params
    args = request.args.to_dict()
    given_hash = args['hash']
    del args['hash']
    args_string = '\n'.join(sorted([f"{k}={v}" for k, v in args.items()]))
    computed_hash = hmac.new(hashlib.sha256(BOT_TOKEN.encode()).digest(), args_string.encode(), hashlib.sha256).hexdigest()
    if given_hash != computed_hash:
        return "Bad hash", 403
    if int(time.time()) - int(args["auth_date"]) > 86400:
        return "Data is outdated", 403
    
    for k, v in TEAMS.items():
        if args.get('username', '').strip("@") in v or args["id"] in v:
            return send_file(f'/archives/{k}.zip', download_name=f'{k}.zip')
    return f"No teams for user {args.get('username', '')} | {args['id']}"
