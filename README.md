# Как запустить у себя
1. Форкните репу
2. Отредачьте cs.conf как вам надо
3. Если не нужен вайргард, выпилите его из .gitlab-ci.yml (все строчки где есть одновременно слова docker и wg) и из docker-compose.yml (сервис wireguard)
4. Закиньте нужные файлы в private (список ниже)
5. В Settings -> CI/CD -> Variables добавьте переменные (список ниже)
6. Нажмите Build -> Pipelines -> Run Pipeline, оно задеплоится (там также есть ручные джобы для управления жюрейкой)

# Как хостить
Я использую DigitalOcean, поднимаю там сетку 192.168.100.0/24 и закидываю дроплеты, сначала жюрейку на .2, потом вулнбоксы по очереди (из заранее подготовленного снапшота):
```bash
printf "hleb zulim saviors eyja pupseni vupseni 4kurs swarm pudgers iphone suhar" | xargs -n1 -I TEAM -d " " doctl compute droplet create TEAM --image 104246752 --size s-1vcpu-2gb --region fra1 --ssh-keys "de:3c:cb:21:1a:a3:5e:88:ce:76:87:ce:2d:61:fe:d3,03:1f:61:10:4d:76:81:18:c3:38:bc:29:68:f6:37:c6" --enable-monitoring --enable-private-networking
```

Обратите внимание, что жюрейка деплоится по схеме https://gitlab.com/deploy-by-example, вам нужно сначала туда накатить на дроплет ansible-docker и получить DEPLOY_CONFIG.

# Переменные окружения
ADMIN_AUTH -- логин:пароль от жюрейской админки (/admin урл), например root:1234

DEPLOY_CONFIG -- креды для TLS-аутентификации в докере (артефакт ansible-docker)

DOZZLE_PASSWORD -- пароль от морды для просмотра логов контейнеров (/logs урл)

FLAGS_SECRET -- ключ hmac для генерации флагов, любая строка

TARGET_HOST -- на каком домене или айпишнике будет всё висеть, нужно для траефика

TEAM_TOKEN_SEED -- ключ hmac для генерации токенов команд, любая строка

# Файлы в private
cs.private.conf -- приватная часть конфига жюрейки, переписывает основной конфиг

Пример:
```
{
  services => [
        {name => 'reports',    path => '/app/checkers/reports/checker.py',    timeout => 60},
        {name => 'tavern',    path => '/app/checkers/tavern.py',    timeout => 60},
  ],
}
```

checkers/ -- тут лежат чекеры, путь до которых прописывается в конфиге жюрейки

jury.conf -- вайргард конфиг жюрейки, через него ходят команды на вулнбоксы, пример:
```
[Interface]
PrivateKey = <private_key>
Address = 10.13.37.2/24

PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -d 192.168.100.0/24 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -d 192.168.100.0/24 -j MASQUERADE

[Peer]
PublicKey = yo8SML5t6G8j7o5AAg5tclvCbN+JomNn6faZH1CSfR8=
AllowedIPs = 10.13.37.0/24
Endpoint = vpn.sibears.ru:51820
PersistentKeepalive = 25
```
